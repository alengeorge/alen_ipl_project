const { filterDataByYear } = require("./extraRunsconcededPerTeam");
const { matches, deliveries } = require("./index");

const economyDataCalculator = (filterData) => {
  let economyData = {};
  const checkifBowlerExist = (obj) => {
    if (!economyData.hasOwnProperty(obj.bowler)) {
      economyData[obj.bowler] = [1, obj.total_runs];
      return false;
    }
    return true;
  };

  for (let obj of filterData) {
    if (checkifBowlerExist(obj)) {
      economyData[obj.bowler][0] += 1;
      economyData[obj.bowler][1] += obj.total_runs;
    }
  }

  return economyData;
};

function topEconomicalBowlers(matches, deliveries, year) {
  let filteredData = filterDataByYear(matches, deliveries, year);

  let economicalDataEachBowler = economyDataCalculator(filteredData);

  let calculateEconomyRate = (economicalDataEachBowler)=>{

    for(let obj in economicalDataEachBowler){
        economicalDataEachBowler[obj][0]=parseInt(economicalDataEachBowler[obj][0]/6)+(economicalDataEachBowler[obj][0]%6*.1)
        economicalDataEachBowler[obj]=economicalDataEachBowler[obj].reduceRight((acc,elem)=> +(acc/elem).toFixed(2))
    }
    return economicalDataEachBowler
  }

  let economyOfBowlers = calculateEconomyRate(economicalDataEachBowler)

  

  return Object.entries(economyOfBowlers).sort((a,b)=> a[1]-b[1]).slice(0,10);
}

module.exports={topEconomicalBowlers}
// console.log(topEconomicalBowler(matches, deliveries, 2015));
