let { matches } = require("./index");
function getMatchesWonPerTeamPerYear(matches) {
  let matchesWonData = {};

  const checkYearAdded = (obj) => {
    if (!matchesWonData[obj.winner].hasOwnProperty(obj.season)) {
      matchesWonData[obj.winner][obj.season] = 1;
      return false;
    }
    return true;
  };

  const CheckTeamAdded = (obj) => {
    if (!matchesWonData.hasOwnProperty(obj.winner)) {
      matchesWonData[obj.winner] = { [obj.season]: 1 };
      return false;
    }
    return true;
  };

  for (let obj of matches) {
    if (!(obj.winner && obj.season)) {
      continue;
    }
    if (CheckTeamAdded(obj) && checkYearAdded(obj)) {
      matchesWonData[obj.winner][obj.season] += 1;
    }
  }

  return matchesWonData;
}

module.exports = { getMatchesWonPerTeamPerYear };

// console.log(getMatchesWonPerTeamPerYear(matches));
