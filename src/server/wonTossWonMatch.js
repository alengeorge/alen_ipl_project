const { matches } = require(".");

function getWonTossWonMatchPerTeam(matches) {
  let tossWonMatchWon = {};

  const checkIfTeamExist = (team) => {
    if (!tossWonMatchWon.hasOwnProperty(team)) {
      tossWonMatchWon[team] = 1;
      return false;
    }
    return true;
  };

  matches.forEach((element) => {
    if (
      element.toss_winner === element.winner &&
      checkIfTeamExist(element.winner)
    ) {
      tossWonMatchWon[element.winner] += 1;
    }
  });

  return tossWonMatchWon
}

// console.log(getWonTossWonMatchPerTeam(matches));

module.exports={getWonTossWonMatchPerTeam}