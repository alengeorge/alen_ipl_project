let { deliveries, matches } = require("./index");

const filterDataByYear = (matches,deliveries,year) => {
  let filteredData = matches
    .filter((obj) => obj.season === year)
    .map((obj) => obj.id);

  let filteredData_deliveries = deliveries.filter((obj) =>
    filteredData.includes(obj.match_id)
  );

  return filteredData_deliveries;
};

function getExtraRunsConcededByEachTeamByYear(deliveries, matches, year) {
  let filteredData = filterDataByYear(matches, deliveries, year);

  let extrasConceded = {};

  const checkTeamAdded = (obj) => {
    if (!extrasConceded.hasOwnProperty(obj.bowling_team)) {
      extrasConceded[obj.bowling_team] = obj.extra_runs;
      return false;
    }
    return true;
  };
  for (let obj of filteredData) {
    if (checkTeamAdded(obj)) {
      extrasConceded[obj.bowling_team] += obj.extra_runs;
    }
  }

  return extrasConceded;
}

module.exports = { getExtraRunsConcededByEachTeamByYear, filterDataByYear};
// console.log(getExtraRunsConcededByEachTeam(deliveries, matches,2017));
