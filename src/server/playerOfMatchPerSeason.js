const { matches } = require(".");

function getPlayerOfMatchPerSeason(matches) {
  let seasonPOMData = {};

  const checkIfYearPlayerExists = (element) => {
    if (!seasonPOMData.hasOwnProperty(element.season)) {
      seasonPOMData[element.season] = { [element.player_of_match]: 1 };
      return false;
    } else if (
      !seasonPOMData[element.season].hasOwnProperty(element.player_of_match)
    ) {
      seasonPOMData[element.season][element.player_of_match] = 1;
      return false;
    }
    return true;
  };

  matches.forEach((element) => {
    if (checkIfYearPlayerExists(element)) {
      seasonPOMData[element.season][element.player_of_match] += 1;
    }
  });

  for (let obj in seasonPOMData) {
    let yearWiseList = Object.entries(seasonPOMData[obj]);

    let player = yearWiseList.sort((a, b) => a[1] - b[1])[0][0];
    seasonPOMData[obj] = player;
  }

  return seasonPOMData;
}

// console.log(getPlayerOfMatchPerSeason(matches))

module.exports = { getPlayerOfMatchPerSeason };
