let {matches} = require("./index");

function sortByYear(matches) {
   return matches.sort((a, b) => a.season - b.season);
 }

function getMatchCountByYear(matches) {
  let sortedList=sortByYear(matches);

  const yearList = sortedList.map((a) => a.season);

  let count = {};

  for (let elem of yearList) {
    if (count.hasOwnProperty(elem)) {
      count[elem] += 1;
    } else if (elem == undefined) {
    } else {
      count[elem] = 1;
    }
  }

  return count;
}


module.exports={getMatchCountByYear,sortByYear}

// console.log(getMatchCountByYear(matches))
