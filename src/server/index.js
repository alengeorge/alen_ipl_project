const Fs = require("fs");

let csvToJson = require("convert-csv-to-json");
const { getMatchCountByYear } = require("./matchCountByYear");
const { getMatchesWonPerTeamPerYear } = require("./matchesWonPerTeamPerYear");
const { getExtraRunsConcededByEachTeamByYear } = require("./extraRunsconcededPerTeam");
const { topEconomicalBowlers } = require("./economicalBowlers");
const { getWonTossWonMatchPerTeam } = require("./wonTossWonMatch");
const { getPlayerOfMatchPerSeason } = require("./playerOfMatchPerSeason");


let matches = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("src/data/matches.csv");
let deliveries = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("src/data/deliveries.csv");

const dumpToOutput = (data, filename) => {
  const PATH = "src/public/output/";
  Fs.writeFile(PATH+filename + ".json", data, (err) => {
    if (err) {
      throw err;
    }
    console.log("JSON data is saved.");
  });
};

/*
//Number of matches played per year for all the years in IPL.
const totalMatchData = JSON.stringify(getMatchCountByYear(matches));
dumpToOutput(totalMatchData,'totalMatchCountByYear')

//Number of matches won per team per year in IPL.
const matchesWonTeamData = JSON.stringify(getMatchesWonPerTeamPerYear(matches))
dumpToOutput(matchesWonTeamData,'MatchWonTeamPerYear')

//Extra runs conceded per team in the year 2016
let year = 2016
const extrasConceded = JSON.stringify(getExtraRunsConcededByEachTeamByYear(deliveries,matches,year))
dumpToOutput(extrasConceded,'ExtrasConcededON'+year)

//Top 10 economical bowlers in the year 2015
year = 2015
const top10EconomyBowlers = JSON.stringify(topEconomicalBowlers(matches,deliveries,year))
dumpToOutput(top10EconomyBowlers,'Top10EconomyBowlers'+year)

//Find the number of times each team won the toss and also won the match
const wonTossWonMatch = JSON.stringify(getWonTossWonMatchPerTeam(matches))
dumpToOutput(wonTossWonMatch,'wonTossWonMatch')
*/

//Find a player who has won the highest number of Player of the Match awards for each season


const playerOfMatchPerSeason = JSON.stringify(getPlayerOfMatchPerSeason(matches))
dumpToOutput(playerOfMatchPerSeason,'PlayerOfMatchPerSeason')




/*
let inputStream = Fs.createReadStream("data/matches.csv", "utf8");

let matches = [];
inputStream
  .pipe(CsvReadableStream({ parseNumbers: true}))
  .on("data", (row) => matches.push(row))
  .on('end',() => console.log('No more rows!')); */

module.exports = {matches,deliveries};
